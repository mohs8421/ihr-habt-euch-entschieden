Ihr habt euch entschieden!

Ich habe es heraus gefunden,  
habe euch gesagt, was aus eurem handeln folgt.  
Ich habe geforscht und alles abgewogen.  
Es war euch egal so lang es euch Reichtum bringt.

Ihr habt euch für das Geld entschieden!

Ich habe euch gewarnt, wohin das alles führt,  
dass so vieles auf der Welt stirbt für euer Geld,  
dass da bald auch nichts mehr wächst auf eurem Feld.  
Ihr habt trotzdem Gift auf die Pflanzen gesprüht.

Ihr habt euch für das Gift entschieden!

Ich hab gemahnt, nicht mehr in Dosen zu fahren,  
dass die Städte und Dörfer drunter leiden,  
dass ihr auch eure eigenen Kinder überfahrt.  
Ihr habt alles asphaltiert.

Ihr habt euch für die Dosen entschieden!

Ich wollte nicht, dass ihr auch meine Kinder tötet.  
So habe ich eure Dosen blockiert.  
Das hat euch nicht gefallen. DAS hat euch radikalisiert?  
Ihr habt sonst alles ignoriert.

Ihr habt euch für den Tod entschieden!

Ihr wusstet, wie es läuft, dass dann alles schlimmer wird.  
Jetzt sitzen wir hier und spielen Russisches Roulette mit dem Wetter.  
Drei Kammern gefüllt mit Dürre, zwei mit zu viel Wasser.  
Ihr gebt uns die Schuld, wir hätten ja nichts gemacht.

Ihr habt euch für die Flut entschieden!


Mohs

CC BY-NC-SA 4.0 
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/mohs8421/ihr-habt-euch-entschieden">Ihr habt euch entschieden</a> by <span property="cc:attributionName">Mohs</span> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 